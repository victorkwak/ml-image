cd xgboost && \
mkdir build && \
cd build && \
cmake .. -DPLUGIN_UPDATER_GPU=ON && \
make -j && \
cd ../python-package && \
python3 setup.py install && \
echo "export PYTHONPATH=~/xgboost/python-package" > ~/.bashrc
source ~/.bashrc
